# openid-test

Project which helps you to launch a openid server populated with a few users.
Currently it populates 1 user.

## Getting started

- activate the environment variable of the `xuc` in the file `/etc/docker/compose/docker-xivocc.yml`

```
    - OIDC_SERVER_URL=https://openid.test.avencall.com:8443/auth/realms/xuc
    - ENABLE_OIDC=true
    - OIDC_CLIENT_ID=xuc
    - OIDC_AUDIENCE=xuc
    - CONFIG_FILE=/conf/xuc.conf
```

- activate the environment variable of the `xucmgt` in the file `/etc/docker/compose/docker-xivocc.yml`

```
    - OIDC_SERVER_URL=https://openid.test.avencall.com:8443/auth/realms/xuc
    - ENABLE_OIDC=true
    - OIDC_CLIENT_ID=xuc
    - OIDC_LOGOUT_ENABLE=true
```

- run `xivocc-dcomp up -d`

- add `openid.test.avencall.com` to your local `/etc/hosts`

- execute the `cypress/containers/openid/install.sh` and `cypress/containers/ldap/install.sh` files

- add this flags to the xucmgt running command

```
    -Dxuc.openidServerUrl=https://openid.test.avencall.com:8443/auth/realms/xuc
    -Dxuc.openidClientId=xuc
    -Doidc.enable=true
```
